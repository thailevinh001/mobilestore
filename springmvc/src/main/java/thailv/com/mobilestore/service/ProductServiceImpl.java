package thailv.com.mobilestore.service;

import thailv.com.mobilestore.entities.Product;
import thailv.com.mobilestore.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository repository;

    @Override
    public Product getProductByCode(int code) {
        return repository.findById(code).orElse(null);
    }

    @Override
    public List<Product> getProducts() {
        return repository.findAll();
    }

    @Override
    public Product addProduct(Product product) {
        return repository.save(product);
    }

    @Override
    public void updateProducts(List<Product> products) {
        repository.saveAll(products);
    }
}
