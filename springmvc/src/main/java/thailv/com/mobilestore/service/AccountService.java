package thailv.com.mobilestore.service;

import thailv.com.mobilestore.entities.Account;

public interface AccountService {
    Account checkLogin(String username, String password);
}
