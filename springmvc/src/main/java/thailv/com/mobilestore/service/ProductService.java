package thailv.com.mobilestore.service;

import thailv.com.mobilestore.entities.Product;

import java.util.List;

public interface ProductService {

    Product getProductByCode(int code);

    List<Product> getProducts();

    Product addProduct(Product product);

    void updateProducts(List<Product> products);
}
