package thailv.com.mobilestore.service;

import thailv.com.mobilestore.entities.Order;
import thailv.com.mobilestore.entities.OrderDetail;
import thailv.com.mobilestore.entities.Product;

import java.util.List;

public interface OrderDetailsService {

    List<OrderDetail> createOrderDetails(Order order);

    boolean addToCart(Product product);

    boolean removeItemInCart(int productCode);

    boolean clearCart();

    List<OrderDetail> getCart();

    double getCartTotal();

    List<Product> upateProductsInStock();
}
