package thailv.com.mobilestore.service;

import thailv.com.mobilestore.entities.Order;
import thailv.com.mobilestore.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderRepository repository;

    @Override
    public Order createOrder(Order order) {
        return repository.save(order);
    }

    @Override
    public Order getNewestOrder() {
        List<Order> list = repository.findAll();
        return list.get(list.size()-1);
    }
}
