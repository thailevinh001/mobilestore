package thailv.com.mobilestore.service;

import thailv.com.mobilestore.entities.Order;

public interface OrderService {
    Order createOrder(Order order);

    Order getNewestOrder();
}
