package thailv.com.mobilestore.repository;

import thailv.com.mobilestore.entities.OrderDetail;
import thailv.com.mobilestore.entities.OrderDetailId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, OrderDetailId> {
}