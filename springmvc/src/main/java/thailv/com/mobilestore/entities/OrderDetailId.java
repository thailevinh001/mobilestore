package thailv.com.mobilestore.entities;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailId implements Serializable {
    private static final long serialVersionUID = 5936945607316742506L;

    @Column(name = "orderid", nullable = false)
    private Integer orderid;
    @Column(name = "productCode", nullable = false)
    private Integer productCode;
}