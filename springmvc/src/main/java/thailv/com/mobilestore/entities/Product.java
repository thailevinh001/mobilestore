package thailv.com.mobilestore.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "code", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 40)
    private String name;

    @Column(name = "description", nullable = false, length = 260)
    private String description;

    @Column(name = "image", length = 1050)
    private String image;

    @Column(name = "unitPrice", nullable = false, precision = 19, scale = 4)
    private double unitPrice;

    @Column(name = "stock", nullable = false)
    private Integer stock;

    @Column(name = "manufacturer", nullable = false, length = 40)
    private String manufacturer;

    @Column(name = "condition", nullable = false, length = 20)
    private String condition;
}